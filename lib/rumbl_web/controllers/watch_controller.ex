defmodule RumblWeb.WatchController do
  use RumblWeb, :controller
  alias Rumbl.Core

  def action(conn, _) do
    apply(__MODULE__, action_name(conn),
      [conn, conn.params, conn.assigns.current_user])
  end

  def show(conn, %{"id" => id}, user) do
    video = Core.get_user_video!(user, id)
    render conn, "show.html", video: video
  end
end
