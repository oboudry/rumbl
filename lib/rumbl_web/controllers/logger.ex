defmodule RumblWeb.Log do
  # use RumblWeb, :controller
  use Phoenix.Controller, namespace: RumblWeb
  # alias RumblWeb.Router.Helpers
  require Logger

  def init(opts) do
    Keyword.fetch!(opts, :msg)
  end

  def call(conn, msg) do
    Logger.warn("#{msg}: #{inspect(conn.assigns)}")
    conn
  end
end

