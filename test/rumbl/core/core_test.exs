defmodule Rumbl.CoreTest do
  use Rumbl.DataCase
  import Rumbl.TestHelpers

  alias Rumbl.Core

  describe "videos" do
    alias Rumbl.Core.Video

    @valid_attrs %{description: "some description", title: "some title", url: "some url"}
    @update_attrs %{description: "some updated description", title: "some updated title", url: "some updated url"}
    @invalid_attrs %{description: nil, title: nil, url: nil}

    def video_fixture(attrs \\ %{}) do
      user = insert_user()
      attrs = attrs |> Enum.into(@valid_attrs)
      {:ok, video} = Core.create_video(user, attrs)

      video
    end

    test "list_videos/0 returns all videos" do
      video = video_fixture()
      assert Core.list_videos() == [video]
    end

    test "get_video!/1 returns the video with given id" do
      video = video_fixture()
      assert Core.get_video!(video.id) == video
    end

    test "create_video/1 with valid data creates a video" do
      user = insert_user()
      assert {:ok, %Video{} = video} = Core.create_video(user, @valid_attrs)
      assert video.description == "some description"
      assert video.title == "some title"
      assert video.url == "some url"
    end

    test "create_video/1 with invalid data returns error changeset" do
      user = insert_user()
      assert {:error, %Ecto.Changeset{}} = Core.create_video(user, @invalid_attrs)
    end

    test "update_video/2 with valid data updates the video" do
      video = video_fixture()
      assert {:ok, video} = Core.update_video(video, @update_attrs)
      assert %Video{} = video
      assert video.description == "some updated description"
      assert video.title == "some updated title"
      assert video.url == "some updated url"
    end

    test "update_video/2 with invalid data returns error changeset" do
      video = video_fixture()
      assert {:error, %Ecto.Changeset{}} = Core.update_video(video, @invalid_attrs)
      assert video == Core.get_video!(video.id)
    end

    test "delete_video/1 deletes the video" do
      video = video_fixture()
      assert {:ok, %Video{}} = Core.delete_video(video)
      assert_raise Ecto.NoResultsError, fn -> Core.get_video!(video.id) end
    end

    test "change_video/1 returns a video changeset" do
      video = video_fixture()
      assert %Ecto.Changeset{} = Core.change_video(video)
    end
  end

  describe "categories" do
    alias Rumbl.Core.Category

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def category_fixture(attrs \\ %{}) do
      {:ok, category} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Core.create_category()

      category
    end

    test "list_categories/0 returns all categories" do
      category = category_fixture()
      # assert Core.list_categories() == [category]
      assert Enum.member?(Core.list_categories(), category)
    end

    test "get_category!/1 returns the category with given id" do
      category = category_fixture()
      assert Core.get_category!(category.id) == category
    end

    test "create_category/1 with valid data creates a category" do
      assert {:ok, %Category{} = category} = Core.create_category(@valid_attrs)
      assert category.name == "some name"
    end

    test "create_category/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Core.create_category(@invalid_attrs)
    end

    test "update_category/2 with valid data updates the category" do
      category = category_fixture()
      assert {:ok, category} = Core.update_category(category, @update_attrs)
      assert %Category{} = category
      assert category.name == "some updated name"
    end

    test "update_category/2 with invalid data returns error changeset" do
      category = category_fixture()
      assert {:error, %Ecto.Changeset{}} = Core.update_category(category, @invalid_attrs)
      assert category == Core.get_category!(category.id)
    end

    test "delete_category/1 deletes the category" do
      category = category_fixture()
      assert {:ok, %Category{}} = Core.delete_category(category)
      assert_raise Ecto.NoResultsError, fn -> Core.get_category!(category.id) end
    end

    test "change_category/1 returns a category changeset" do
      category = category_fixture()
      assert %Ecto.Changeset{} = Core.change_category(category)
    end
  end
end
