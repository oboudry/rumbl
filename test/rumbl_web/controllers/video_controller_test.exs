defmodule RumblWeb.VideoControllerTest do
  use RumblWeb.ConnCase

  import Rumbl.TestHelpers

  alias Rumbl.Core

  @create_attrs %{description: "some description", title: "some title", url: "some url"}
  @update_attrs %{description: "some updated description", title: "some updated title", url: "some updated url"}
  @invalid_attrs %{description: nil, title: nil, url: nil}

  def fixture(user, :video) do
    # user = insert_user()
    {:ok, video} = Core.create_video(user, @create_attrs)
    video
  end

  describe "index" do
    setup [:login_user]
    
    test "lists all videos", %{conn: conn} do
      conn = get conn, video_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Videos"
    end

    test "lists all user's videos on index", %{conn: conn, user: user} do
      user_video = insert_video(user, title: "funny cats")
      other_video = insert_video(insert_user(username: "other"), title: "another video")

      conn = get conn, video_path(conn, :index)
      assert Regex.match?(~r/Listing Videos/, html_response(conn, 200))
      assert String.contains?(conn.resp_body, user_video.title)
      refute String.contains?(conn.resp_body, other_video.title)
    end
  end

  describe "new video" do
    setup [:login_user]

    test "renders form", %{conn: conn} do
      conn = get conn, video_path(conn, :new)
      assert html_response(conn, 200) =~ "New Video"
    end
  end

  describe "create video" do
    setup [:login_user]

    test "redirects to show when data is valid", %{conn: conn, user: user} do
      attrs = Map.put(@create_attrs, :user_id, user.id)
      conn = post conn, video_path(conn, :create), video: attrs
 
      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == video_path(conn, :show, id)

      # require IEx; IEx.pry
      conn = recycle_assigns(conn)
      conn = get conn, video_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Video"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, video_path(conn, :create), video: @invalid_attrs
      assert html_response(conn, 200) =~ "New Video"
    end
  end

  describe "edit video" do
    setup [:login_user, :create_video]

    test "renders form for editing chosen video", %{conn: conn, video: video} do
      conn = get conn, video_path(conn, :edit, video)
      assert html_response(conn, 200) =~ "Edit Video"
    end
  end

  describe "update video" do
    setup [:login_user, :create_video]

    test "redirects when data is valid", %{conn: conn, video: video, user: user} do
      attrs = Map.put(@update_attrs, :user_id, user.id)
      conn = put conn, video_path(conn, :update, video), video: attrs
      assert remove_slug(redirected_to(conn)) == remove_slug(video_path(conn, :show, video))

      conn = recycle_assigns(conn)
      conn = get conn, video_path(conn, :show, video)
      assert html_response(conn, 200) =~ "some updated description"
    end

    test "renders errors when data is invalid", %{conn: conn, video: video} do
      conn = put conn, video_path(conn, :update, video), video: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Video"
    end
  end

  describe "delete video" do
    setup [:login_user, :create_video]

    test "deletes chosen video", %{conn: conn, video: video} do
      conn = delete conn, video_path(conn, :delete, video)
      assert redirected_to(conn) == video_path(conn, :index)
      assert_error_sent 404, fn ->
        conn = recycle_assigns(conn)
        get conn, video_path(conn, :show, video)
      end
    end
  end

  describe "require authentication" do
    test "requires user authentication on all actions", %{conn: conn} do
      Enum.each([
        get(conn, video_path(conn, :new)),
        get(conn, video_path(conn, :index)),
        get(conn, video_path(conn, :show, "123")),
        get(conn, video_path(conn, :edit, "123")),
        get(conn, video_path(conn, :update, "123")),
        get(conn, video_path(conn, :create, %{})),
        get(conn, video_path(conn, :delete, "123")),
      ], fn conn ->
        assert html_response(conn, 302)
        assert conn.halted
      end)
    end
  end

  defp login_user(_) do
    user = insert_user(username: "max")
    conn = assign(build_conn(), :current_user, user)
    {:ok, conn: conn, user: user}
  end

  defp create_video(%{user: user}) do
    video = fixture(user, :video)
    {:ok, video: video}
  end

  defp recycle_assigns(conn) do
    saved_assigns = conn.assigns
    conn
      |> recycle()
      |> (&Map.put(&1, :assigns, saved_assigns)).()
  end

  defp remove_slug(url) do
    Regex.replace(~r{(^.*\d+)[^/]+$}, url, "\\1")
  end
end
