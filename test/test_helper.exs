ExUnit.start()

Ecto.Adapters.SQL.Sandbox.mode(Rumbl.Repo, :manual)

# Mix.Tasks.Run.run(["priv/repo/seeds.exs"])
System.cmd "mix", ["run", "priv/repo/seeds.exs"]

